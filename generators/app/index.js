const { lstatSync, readdirSync, existsSync } = require("fs");
const { join, basename } = require('path');
const equal = require('deep-equal');
const gulp = require('gulp');
const ts = require('gulp-typescript');
const babel = require('gulp-babel');
const prettier = require('prettier');
const Generator = require('yeoman-generator');
module.exports = class extends Generator {
    constructor(args, opts) {
        super(args, opts);
        this._defineOptions();
        this.currentSchemaMap = {};
        this.migrationDirs = []; // [{ prefix, dirname}, ...]
        this.hasSchemaChangeOccurred = false;
        this.isTypescript = this.options.isTypescript;
        this.srcDirPath = this.destinationPath(this.options.sourceDir);
        this.srcMigrationDirPath = `${this.srcDirPath}${this.options.migrationDir}`;
        this.tempDirPath = this.destinationPath(this.options.tempDir);
        this.tempModelDirPath = `${this.tempDirPath}${this.options.modelDir}`;
        this.tempMigrationDirPath = `${this.tempDirPath}${this.options.migrationDir}`;
        // Can't output files to temp dir of generator because modules might not exist. Don't want limitations on how objectSchema is created!!!
        // this.tempDirPath = join(__dirname, 'temp/');
        // this.tempModelDirPath = join(this.tempDirPath, this.options.modelDir);
        // this.tempMigrationDirPath = join(this.tempDirPath, this.options.migrationDir);
    }

    _defineOptions() {
        this.option('isTypescript', {
           desc: 'Flag for indicating if project is Typescript or not.',
           type: Boolean,
           default: false,
           hide: false,
        });
        this.option('migrationDir', {
            desc: 'The directory where the scaffolded migrations will reside in the sourceDir.',
            type: String,
            default: 'migrations/',
            hide: false,
        });
        this.option('sourceDir', {
            desc: 'The director where schemaDir and modelDir reside underneath',
            alias: 's',
            type: String,
            default: 'src/',
            hide: false,
        });
        this.option('modelDir', {
            desc: 'The directory where the Models with most current schemas reside in the sourceDir.',
            alias: 'm',
            type: String,
            default: 'models/',
            hide: false,
        });
        this.option('tempDir', {
            desc: 'The directory in root of project using the generator where temp files will go',
            alias: 't',
            type: String,
            default: 'temp/',
            hide: false
        });
        this.option('schemaVersion', {
            desc: 'Explicitly specify the schema version number. Good for when your first saved schema version is greater than 0.',
            type: Number,
            default: 0,
            hide: false
        });
        this.option('schemaDesc', {
            desc: 'Describe the migration that will be created.',
            type: String,
            default: 'default',
        });
        this.option('objectSchemaFileSuffix', {
            desc: "Suffix to use if the object schema's name differs from the filename(i.e. added suffix)",
            type: String,
            default: '',
            hide: false
        })
    }

    _extension(file) {
        if (this.isTypescript) {
            return file + '.ts';
        }
        return file + '.js';
    }

    _logPathVars() {
        this.log([
            this.srcDirPath,
            this.srcMigrationDirPath,
            this.tempDirPath,
            this.tempModelDirPath,
            this.tempMigrationDirPath
        ]);
    }

    _isNumeric(val) {
        return !isNaN(parseFloat(val)) && isFinite(val);
    }

    /**
     * Determines if the schema has changed by loading the
     * current schema and then comparing it to the last migration dirs schema.
     * If no migration dirs then the schema has changed, since it now exists.
     * @returns {Promise<any>}
     */
    initializing() {
        return this._createTempDir()
        .then(() => {
            this._loadCurrentSchemaMapFromTempDir();
            if (existsSync(this.tempMigrationDirPath)) {
                this._loadMigrationDirsFromTempDir();
                this._loadHasSchemaChanged();
            }
            this.fs.delete(this.tempDirPath);
        });
    }

    /**
     * Creates a Node.js require compatible copy of the project where generator is executed.
     * @returns {Promise<any>}
     * @private
     */
    _createTempDir() {
        return new Promise((resolve, reject) => {
            let stream = gulp.src(this._extension(`${this.srcDirPath}**/*`))
            if (this.isTypescript) {
                return stream
                    .pipe(ts({
                        "target": "es2015",
                        "module": "es2015",
                        "lib": ["es2015"],
                        "moduleResolution": "node",
                        "experimentalDecorators": true,
                        "allowSyntheticDefaultImports": true,
                        "noImplicitAny": false,
                        "preserveConstEnums": true,
                        "allowJs": false,
                        "noImplicitReturns": true,
                        "noUnusedParameters": true,
                        "noUnusedLocals": true,
                        "rootDir": this.options.sourceDir
                    }))
                    .pipe(babel({ presets: ['babel-preset-es2015'].map(require.resolve) }))
                    .pipe(gulp.dest(this.tempDirPath))
                    .on('error', reject)
                    .on('end', resolve)
                ;
            }
            return stream
                .pipe(babel())
                .pipe(gulp.dest(this.tempDirPath))
                .on('error', reject)
                .on('end', resolve)
            ;
        })
    }

    _loadCurrentSchemaMapFromTempDir() {
        readdirSync(this.tempModelDirPath).forEach((file) => {
            const exports = require(this.tempModelDirPath + file);
            const extensionIndex = file.lastIndexOf('.');
            const namedExport = file.slice(0, extensionIndex);
            if (namedExport !== 'index') {
                const schema = exports[namedExport].schema;
                this.currentSchemaMap[namedExport] = schema;
            }
        });
    }

    _loadMigrationDirsFromTempDir() {
        this.migrationDirs = readdirSync(this.tempMigrationDirPath)
            .filter((filename) => {
                return lstatSync(join(this.tempMigrationDirPath, filename)).isDirectory();
            })
            .map((dirname) => {
                const indexOfFirstHyphen = dirname.indexOf('-');
                let prefix = dirname.slice(0, indexOfFirstHyphen);
                if (indexOfFirstHyphen === -1 || !this._isNumeric(prefix)) {
                    return null;
                }
                prefix = +prefix;
                return { prefix, dirname };
            })
            .filter((dirRep) => dirRep !== null)
            .sort(function (rep1, rep2) { return rep1.prefix - rep2.prefix; })
        ;
    }

    /**
     * Determines how the current schema different from the last.
     * Will determine what object schema's have been added, removed, or updated.
     * @private
     */
    _loadHasSchemaChanged() {
        if (this.migrationDirs.length === 0) {
            this.log('FIRST MIGRATION');
            this.hasSchemaChangeOccurred = true;
            return;
        }
        const lastMigrationDir = this.migrationDirs[this.migrationDirs.length - 1];
        const lastMigrationDirPath = join(this.tempMigrationDirPath, lastMigrationDir.dirname);
        const lastMigration = require(`${lastMigrationDirPath}/index.js`);
        const lastMigrationSchemas = lastMigration.default.schema;
        // Changed or Missing Ones
        for (const originalSchema of lastMigrationSchemas) {
            const objectName = originalSchema.name;
            const objectSchemaFileName = `${objectName}${this.options.objectSchemaFileSuffix}`;
            const updatedSchema = this.currentSchemaMap[objectSchemaFileName];
            // Changed
            if ( updatedSchema !== undefined) {
                const hasSchemaChanged = !equal(originalSchema, updatedSchema);
                if (hasSchemaChanged) {
                    this.log(`${objectSchemaFileName} Object schema has changed.`);
                    this.hasSchemaChangeOccurred = true;
                }
            // Missing
            } else {
                this.log(`${objectSchemaFileName} Object Schema has been dropped.`);
                this.hasSchemaChangeOccurred = true;
            }
        }
        // Added
        for (const objectSchemaFileName in this.currentSchemaMap) {
            const found = lastMigrationSchemas.find((schema) => {
                return `${schema.name}${this.options.objectSchemaFileSuffix}` === objectSchemaFileName;
            });
            if (!found) {
                this.log(`${objectSchemaFileName} Object Schema has been added.`);
                this.hasSchemaChangeOccurred = true;
            }
        }
    }

    configuring() {
        this.config.save();
    }
    writing() {
        //1. Don't create migration if nothing has changed
        if(!this.hasSchemaChangeOccurred) {
            this.log('NO SCHEMA CHANGES.');
            return;
        }
        //2. Create a new migration dir
        this._createMigrationDir();
        //3. Recreate the migrations dir index so that it imports the newly created migration dir
        this.fs.copyTpl(
            this.templatePath('migrationsIndex.ejs'),
            this.destinationPath(this._extension(`${this.srcMigrationDirPath}/index`)),
            { migrationDirs: this.migrationDirs }
        );
    }

    /**
     *  Determine schemaVersion if no migrations, option is not default value, or it's the next ascending schemaVersion
     * @returns {number}
     * @private
     */
    _determineSchemaVersion() {
        let schemaVersion = -1;
        if (this.migrationDirs.length === 0) {
            schemaVersion = 0;
        }
        if (this.options.schemaVersion !== 0) {
            schemaVersion = this.options.schemaVersion;
        }
        if (schemaVersion === -1) {
            const lastMigrationDir = this.migrationDirs[this.migrationDirs.length - 1];
            schemaVersion = lastMigrationDir.prefix + 1;
        }
        return schemaVersion;
    }

    /**
     * Creates a new migration dir based on the currentSchemaMap
     * Also adds a new migration dir entry to the array.
     * @private
     */
    _createMigrationDir() {
        const schemaVersion = this._determineSchemaVersion();
        const dirname = `${schemaVersion}-${this.options.schemaDesc}`;
        const migrationDir = `${this.srcMigrationDirPath}${dirname}`;
        //a. Copy current schema to new migration folder
        for (const schemaName in this.currentSchemaMap) {
            const schema = this.currentSchemaMap[schemaName];
            this.fs.copyTpl(
                this.templatePath('objectSchema.ejs'),
                this.destinationPath(this._extension(`${migrationDir}/${schemaName}`)),
                { schema, prettier }
            );
        }
        //b. Create migration callback file
        this.fs.copyTpl(
            this.templatePath('migration.ejs'),
            this.destinationPath(this._extension(`${migrationDir}/migration`)),
            {}
        );
        //c. Creation migration dir index file to export schemas, schemaVersion, and migration callback
        this.fs.copyTpl(
            this.templatePath('migrationIndex.ejs'),
            this.destinationPath(this._extension(`${migrationDir}/index`)),
            { schemas: Object.keys(this.currentSchemaMap), schemaVersion }
        );
        this.migrationDirs.push({ prefix: schemaVersion, dirname });

    }
};
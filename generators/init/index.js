const { existsSync } = require("fs");
const mkdirp = require('mkdirp');
const Generator = require('yeoman-generator');
module.exports = class extends Generator {
    constructor(args, opts) {
        super(args, opts);
        this.option('isTypescript', {
            desc: 'Flag for indicating if project is Typescript or not.',
            type: Boolean,
            default: false,
            hide: false,
        });
        this.option('migrationDir', {
            desc: 'The directory where the scaffolded migrations will reside in the project.',
            type: String,
            default: 'src/migrations/',
            hide: false,
        });
        this.srcMigrationsDirPath = `${this.options.migrationDir}`;
    }

    _extension(file) {
        if (this.options.isTypescript) {
            return file + '.ts';
        }
        return file + '.js';
    }


    initializing() {
    }

    configuring() {
        this.config.save();
    }
    writing() {
        this._createMigrationsDir();
        this._createMigrationsDirIndex();
    }


    /**
     * Creates a new migrations dir if it doesn't exist
     * @private
     */
    _createMigrationsDir() {
        if (!existsSync(this.srcMigrationsDirPath)) {
            mkdirp.sync(this.srcMigrationsDirPath);
        } else {
            this.log('MIGRATIONS DIR ALREADY EXISTS.')
        }
    }

    /**
     * Create a new migrations dir index.js file with one export:
     *   an empty migrations array since none exist yet
     * @private
     */
    _createMigrationsDirIndex() {
        const mirgrationsIndexPath = this.destinationPath(this._extension(`${this.srcMigrationsDirPath}/index`));
        if(!existsSync(mirgrationsIndexPath)) {
            this.fs.copyTpl(
                this.templatePath('migrationsIndex.ejs'),
                mirgrationsIndexPath,
                {}
            );
        } else {
            this.log('MIGRATIONS DIR index.js ALREADY EXISTS.')
        }
    }
};